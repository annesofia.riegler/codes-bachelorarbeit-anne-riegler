// -*- C++ -*-

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DISKinematics.hh"
#include "Rivet/Projections/DISFinalState.hh"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
//Commands to write data to file 
using namespace std;
using std::cout;
using std::ofstream;
using std::endl;
using std::string;
using std::fstream;



namespace Rivet {
  
  //Files that store x,y, Q^2, and momenta of final state particles in lab and Breit frame
  ofstream JetLabData("jetLabData.csv");
  ofstream JetBreitData("jetBreitData.csv");
	
  

   /// @brief Add a short analysis description here
  class H1tau1b : public Analysis {
  public:
    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(H1tau1b);
    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections. Note that the definition
      // of the scattered lepton can be influenced by sepcifying
      // options as declared in the .info file.
      DISLepton lepton(options());
      declare(lepton, "Lepton");
      declare(DISKinematics(lepton), "Kinematics");
      declare(FinalState(), "FS");
      const DISFinalState& disfs = declare(DISFinalState(DISFinalState::BoostFrame::LAB), "DISFS");

      FastJets jetfs(disfs, FastJets::KT, 1.0, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(jetfs, "jets");

      // Book histograms
	
      book(_hist_Q2, "Q2",logspace(10,0.1, 1000.0));
      book(_hist_y, "y",40,0.,1.);
      book(_hist_x, "xBj",logspace(10,0.00001, 1.0));
      book(_hist_ept, "ept", logspace(6,10,100));
      book(_hist_jetpt, "jetpt", logspace(6,10,100));
      book(_hist_jeteta, "jeteta", 5,-1.0,2.5);
      book(_hist_qt, "jetqt", {0. ,        0.06510462, 0.13933307, 0.27149627, 0.50681187 ,0.92578957,   1.67177624, 3.  });
      _inclusive_xs=0.0;
      book(_hist_dphi, "jetpdhi", {0.     ,    0.05069951, 0.10235664, 0.18708038, 0.32603721 ,0.55394268,	    0.92773432, 1.54079633});

      book(_hist_tau1b10, "tau1b10", 10,0.,1.);
      book(_hist_tau1b,   "tau1b", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});

      book( _hist_tau1b_y0_Q0 ,   "tau1b_y0_Q0", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y0_Q1 ,   "tau1b_y0_Q1", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y0_Q2 ,   "tau1b_y0_Q2", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y0_Q3 ,   "tau1b_y0_Q3", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y0_Q4 ,   "tau1b_y0_Q4", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y0_Q5 ,   "tau1b_y0_Q5", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y0_Q6 ,   "tau1b_y0_Q6", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y0_Q7 ,   "tau1b_y0_Q7", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y0_Q8 ,   "tau1b_y0_Q8", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});

      book( _hist_tau1b_y1_Q0 ,   "tau1b_y1_Q0", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y1_Q1 ,   "tau1b_y1_Q1", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y1_Q2 ,   "tau1b_y1_Q2", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y1_Q3 ,   "tau1b_y1_Q3", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y1_Q4 ,   "tau1b_y1_Q4", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y1_Q5 ,   "tau1b_y1_Q5", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y1_Q6 ,   "tau1b_y1_Q6", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y1_Q7 ,   "tau1b_y1_Q7", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y1_Q8 ,   "tau1b_y1_Q8", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});

      book( _hist_tau1b_y2_Q0 ,   "tau1b_y2_Q0", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y2_Q1 ,   "tau1b_y2_Q1", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y2_Q2 ,   "tau1b_y2_Q2", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y2_Q3 ,   "tau1b_y2_Q3", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y2_Q4 ,   "tau1b_y2_Q4", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y2_Q5 ,   "tau1b_y2_Q5", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y2_Q6 ,   "tau1b_y2_Q6", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y2_Q7 ,   "tau1b_y2_Q7", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y2_Q8 ,   "tau1b_y2_Q8", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});

      book( _hist_tau1b_y3_Q0 ,   "tau1b_y3_Q0", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y3_Q1 ,   "tau1b_y3_Q1", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y3_Q2 ,   "tau1b_y3_Q2", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y3_Q3 ,   "tau1b_y3_Q3", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y3_Q4 ,   "tau1b_y3_Q4", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y3_Q5 ,   "tau1b_y3_Q5", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y3_Q6 ,   "tau1b_y3_Q6", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y3_Q7 ,   "tau1b_y3_Q7", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y3_Q8 ,   "tau1b_y3_Q8", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});

      book( _hist_tau1b_y4_Q0 ,   "tau1b_y4_Q0", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y4_Q1 ,   "tau1b_y4_Q1", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y4_Q2 ,   "tau1b_y4_Q2", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y4_Q3 ,   "tau1b_y4_Q3", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y4_Q4 ,   "tau1b_y4_Q4", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y4_Q5 ,   "tau1b_y4_Q5", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y4_Q6 ,   "tau1b_y4_Q6", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y4_Q7 ,   "tau1b_y4_Q7", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y4_Q8 ,   "tau1b_y4_Q8", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});

      book( _hist_tau1b_y5_Q0 ,   "tau1b_y5_Q0", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y5_Q1 ,   "tau1b_y5_Q1", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y5_Q2 ,   "tau1b_y5_Q2", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y5_Q3 ,   "tau1b_y5_Q3", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y5_Q4 ,   "tau1b_y5_Q4", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y5_Q5 ,   "tau1b_y5_Q5", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y5_Q6 ,   "tau1b_y5_Q6", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y5_Q7 ,   "tau1b_y5_Q7", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      book( _hist_tau1b_y5_Q8 ,   "tau1b_y5_Q8", {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0});
      
      tau1hists = {
         { _hist_tau1b_y0_Q0, _hist_tau1b_y0_Q1, _hist_tau1b_y0_Q2, _hist_tau1b_y0_Q3, _hist_tau1b_y0_Q4, _hist_tau1b_y0_Q5, _hist_tau1b_y0_Q6, _hist_tau1b_y0_Q7, _hist_tau1b_y0_Q8 },
         { _hist_tau1b_y1_Q0, _hist_tau1b_y1_Q1, _hist_tau1b_y1_Q2, _hist_tau1b_y1_Q3, _hist_tau1b_y1_Q4, _hist_tau1b_y1_Q5, _hist_tau1b_y1_Q6, _hist_tau1b_y1_Q7, _hist_tau1b_y1_Q8 },
         { _hist_tau1b_y2_Q0, _hist_tau1b_y2_Q1, _hist_tau1b_y2_Q2, _hist_tau1b_y2_Q3, _hist_tau1b_y2_Q4, _hist_tau1b_y2_Q5, _hist_tau1b_y2_Q6, _hist_tau1b_y2_Q7, _hist_tau1b_y2_Q8 },
         { _hist_tau1b_y3_Q0, _hist_tau1b_y3_Q1, _hist_tau1b_y3_Q2, _hist_tau1b_y3_Q3, _hist_tau1b_y3_Q4, _hist_tau1b_y3_Q5, _hist_tau1b_y3_Q6, _hist_tau1b_y3_Q7, _hist_tau1b_y3_Q8 },
         { _hist_tau1b_y4_Q0, _hist_tau1b_y4_Q1, _hist_tau1b_y4_Q2, _hist_tau1b_y4_Q3, _hist_tau1b_y4_Q4, _hist_tau1b_y4_Q5, _hist_tau1b_y4_Q6, _hist_tau1b_y4_Q7, _hist_tau1b_y4_Q8 },
         { _hist_tau1b_y5_Q0, _hist_tau1b_y5_Q1, _hist_tau1b_y5_Q2, _hist_tau1b_y5_Q3, _hist_tau1b_y5_Q4, _hist_tau1b_y5_Q5, _hist_tau1b_y5_Q6, _hist_tau1b_y5_Q7, _hist_tau1b_y5_Q8 },
      };
    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {
       TestFile << "test" << endl;
	    
      // Get the DIS kinematics
      const DISKinematics& dk = apply<DISKinematics>(event, "Kinematics");
      if ( dk.failed() ) return;
      double x  = dk.x();
      double y  = dk.y();
      double Q2 = dk.Q2();

     
      if (Q2 < 150.0*GeV2) vetoEvent;
      if ( y>0.9 ) vetoEvent;
      if ( y<0.1) vetoEvent; 
      
      const LorentzTransform breitboost = dk.boostBreit();
      const LorentzTransform labboost = breitboost.inverse();
      Jets jets = apply<FastJets>(event, "jets").jetsByPt();   
      
      // Weight of the event
      _hist_Q2->fill(Q2);
      _hist_y->fill(y);
      _hist_x->fill(x);

      // Momentum of the scattered lepton
      const DISLepton& dl = apply<DISLepton>(event,"Lepton");
      if ( dl.failed() ) return;
      const FourMomentum leptonMom = dl.out();
      const double ptel = leptonMom.pT();
      const double enel = leptonMom.E();
      const double thel = leptonMom.angle(dk.beamHadron().mom())/degree;

      if(enel<11*GeV) vetoEvent;
      
      _hist_ept->fill(ptel);


      // Extract the particles other than the lepton
      const FinalState& fs = apply<FinalState>(event, "FS");
      Particles particles;
      particles.reserve(fs.particles().size());
      ConstGenParticlePtr dislepGP = dl.out().genParticle();
      for(const Particle& p: fs.particles()) {
        ConstGenParticlePtr loopGP = p.genParticle();
        if (loopGP == dislepGP) continue;
        particles.push_back(p);
      }

      // four-vectors for tau1b:
      // const FourMomentum P0(920, 0.,0., 920.);
      // const FourMomentum E0(27.6, 0.,0.,-27.6);
      const FourMomentum P0 = dk.beamHadron().mom();
      const FourMomentum E0 = dk.beamLepton().mom();
      const FourMomentum qB = (E0-leptonMom) + x*P0;
      const FourMomentum qJ = x*P0;
     
      //calculate tau^b_1 using eqs. 23 
      double tau1b = 0.; //eq. 23
      for(const Particle& p: particles ) {
         tau1b += std::min( p.dot(qB)   , p.dot(qJ)  ) ;
	 	 if( p.pz() < 0){
		 tau1b2 += p.pz();
         }
      }      
      tau1b *= 2./Q2;
      
//----------------write data to files ------------------------------------------------
      
      FitData << tau1b << ", " << x << ", " << y << ", " << Q2 << endl;


      for(std::vector<int>::size_type i=0; i<jets.size(); i++){
          jets[i].transformBy(breitboost);
          for(std::vector<int>::size_type j=0; j<jets[i].size(); j++){
             JetBreitData << tau1b << ", " << jets[i].eta() << ", " << jets[i].phi() << ", " << jets[i].theta() << ", " << jets[i].pT() << ", " << jets[i].constituents()[j].eta() << ", " << jets[i].constituents()[j].phi() << ", " << jets[i].constituents()[j].theta() << ", " << jets[i].constituents()[j].pT() << ", " << x << ", " << y << ", " << Q2 << endl;
          } 

          jets[i].transformBy(labboost);
          for(std::vector<int>::size_type j=0; j<jets[i].size(); j++){
             JetLabData << tau1b << ", " << jets[i].eta() << ", " << jets[i].phi() << ", " << jets[i].theta() << ", " << jets[i].pT() << ", " << jets[i].constituents()[j].eta() << ", " << jets[i].constituents()[j].phi() << ", " << jets[i].constituents()[j].theta() << ", " << jets[i].constituents()[j].pT() << ", " << x << ", " << y << ", " << Q2 << endl; 
          }
      }



//----------------------------------------------------------------

      // one-dim. histogram
      if ( y>0.2 && y<0.7 )  _hist_tau1b->fill(tau1b);
      
      /// Q2bins:  [150, 200, 282, 447, 708, 1120, 1780, 3550, 10000, 20000]
      /// ybins:   [0.02, 0.05, 0.1, 0.2, 0.4, 0.7, 0.9]
      /// tau1bs:  [0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7,0.8, 0.9, 0.98, 1.0]

      int id_y = -1;
      if ( y < 0.02 ) id_y = -1;
      else if ( y<0.05 ) id_y = 0;
      else if ( y<0.1  ) id_y = 1;
      else if ( y<0.2  ) id_y = 2;
      else if ( y<0.4  ) id_y = 3;
      else if ( y<0.7  ) id_y = 4;
      else if ( y<0.9  ) id_y = 5;

      int id_q = -1;
      if ( Q2 < 150 ) id_q = -1;
      else if ( Q2<200  ) id_q = 0;
      else if ( Q2<282  ) id_q = 1;
      else if ( Q2<447  ) id_q = 2;
      else if ( Q2<708  ) id_q = 3;
      else if ( Q2<1120 ) id_q = 4;
      else if ( Q2<1780 ) id_q = 5;
      else if ( Q2<3550 ) id_q = 6;
      else if ( Q2<10000) id_q = 7;
      else if ( Q2<20000) id_q = 8;

      if ( id_y>=0 && id_q>= 0 ) 
         tau1hists[id_y][id_q]->fill(tau1b);
      
    }


    /// Normalise histograms etc., after the run
    void finalize() {
       
      //scale(_hist_Q2, 1.0/(_inclusive_xs)); // normalize to unity
      ///normalize(_hist_y); // normalize to unity
      //normalize(_hist_ept);
      //scale(_hist_jetpt, 1.0/(_inclusive_xs));
      //normalize(_hist_jeteta);
      //scale(_hist_qt, 1.0/(_inclusive_xs));
      for(auto hs: tau1hists) {
        scale(hs,crossSection()/sumOfWeights());
      }
    }

    //@}

     //Fill histograms 

    /// The histograms.
     //using Histo1DPtr = rivet_shared_ptr< Wrapper< YODA::Histo1D > >
     Histo1DPtr _hist_Q2, _hist_y, _hist_x, _hist_ept, _hist_jetpt, _hist_qt, _hist_jeteta, _hist_dphi;
     Histo1DPtr _hist_tau1b, _hist_tau1b10;
     Histo1DPtr _hist_tau1b_y0_Q0, _hist_tau1b_y0_Q1, _hist_tau1b_y0_Q2, _hist_tau1b_y0_Q3, _hist_tau1b_y0_Q4, _hist_tau1b_y0_Q5, _hist_tau1b_y0_Q6, _hist_tau1b_y0_Q7, _hist_tau1b_y0_Q8;
     Histo1DPtr _hist_tau1b_y1_Q0, _hist_tau1b_y1_Q1, _hist_tau1b_y1_Q2, _hist_tau1b_y1_Q3, _hist_tau1b_y1_Q4, _hist_tau1b_y1_Q5, _hist_tau1b_y1_Q6, _hist_tau1b_y1_Q7, _hist_tau1b_y1_Q8;
     Histo1DPtr _hist_tau1b_y2_Q0, _hist_tau1b_y2_Q1, _hist_tau1b_y2_Q2, _hist_tau1b_y2_Q3, _hist_tau1b_y2_Q4, _hist_tau1b_y2_Q5, _hist_tau1b_y2_Q6, _hist_tau1b_y2_Q7, _hist_tau1b_y2_Q8;
     Histo1DPtr _hist_tau1b_y3_Q0, _hist_tau1b_y3_Q1, _hist_tau1b_y3_Q2, _hist_tau1b_y3_Q3, _hist_tau1b_y3_Q4, _hist_tau1b_y3_Q5, _hist_tau1b_y3_Q6, _hist_tau1b_y3_Q7, _hist_tau1b_y3_Q8;
     Histo1DPtr _hist_tau1b_y4_Q0, _hist_tau1b_y4_Q1, _hist_tau1b_y4_Q2, _hist_tau1b_y4_Q3, _hist_tau1b_y4_Q4, _hist_tau1b_y4_Q5, _hist_tau1b_y4_Q6, _hist_tau1b_y4_Q7, _hist_tau1b_y4_Q8;
     Histo1DPtr _hist_tau1b_y5_Q0, _hist_tau1b_y5_Q1, _hist_tau1b_y5_Q2, _hist_tau1b_y5_Q3, _hist_tau1b_y5_Q4, _hist_tau1b_y5_Q5, _hist_tau1b_y5_Q6, _hist_tau1b_y5_Q7, _hist_tau1b_y5_Q8;
     double _inclusive_xs;
     
     vector<vector<Histo1DPtr> > tau1hists;
     
  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(H1tau1b);


}
  
