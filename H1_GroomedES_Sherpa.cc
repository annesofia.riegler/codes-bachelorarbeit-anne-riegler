// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/DISKinematics.hh"
#include "Rivet/Projections/FastJets.hh" //Not used in GES analysis, no Centauro, useful for others
#include "fastjet/PseudoJet.hh"
#include "Centauro/CentauroPlugin.hh"
#include "Centauro/CentauroPlugin.cc"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
//Commands to write data to file 
using namespace std;
using std::cout;
using std::ofstream;
using std::endl;
using std::string;
using std::fstream;


using namespace fastjet;
using namespace Rivet;
FourMomentum Breit2Lab(FourMomentum Photon, FourMomentum InParticle) // Useful function for boosting to Breit Frame
{
  FourMomentum OutParticle(-100,-100,-100,-100);
  double_t energy, px, py, pz;
  double_t q0,q1,q2,q3;
  double_t Q,qt;
  
  Q  = sqrt( -Photon.dot(Photon) );
  q0 = Photon.E();
  q1 = Photon.px();
  q2 = Photon.py();
  q3 = Photon.pz();
  qt = sqrt(q1*q1+q2*q2);
  
  if(Q == 0.0 || q0 == q3 ) cout << "Boosting Q2 = 0.0 or Y == 0.0 ? " << endl;
  
  if(Q > 0.0 && q0 != q3) {
    
    energy = q0*(InParticle.E() - InParticle.pz())/Q +
      (Q*InParticle.E() - qt*InParticle.px())/(q0-q3);
    
    px = q1*(InParticle.E() - InParticle.pz())/Q -
      (q1*InParticle.px() - q2*InParticle.py())/qt;
    
    py = q2*(InParticle.E() - InParticle.pz())/Q -
      (q2*InParticle.px() + q1*InParticle.py())/qt;
    
    pz = q3*(InParticle.E() - InParticle.pz())/Q +
      (Q*InParticle.E() - qt*InParticle.px())/(q0-q3);
    
    OutParticle.setE(energy);
    OutParticle.setPx(px);
    OutParticle.setPy(py);
    OutParticle.setPz(pz);
  }
  return OutParticle;
}
FourMomentum Lab2Breit(FourMomentum Photon, FourMomentum InParticle)
{
  FourMomentum OutParticle(-100,-100,-100,-100);
  double_t energy, px, py, pz;
  double_t q0,q1,q2,q3;
  double_t Q,qt;

  Q  = sqrt( -Photon.dot(Photon) );
  q0 = Photon.E();
  q1 = Photon.px();
  q2 = Photon.py();
  q3 = Photon.pz();
  qt = sqrt(q1*q1+q2*q2);

  if(Q == 0.0 || q0 == q3 ) cout << "Boosting Q2 = 0.0 or Y == 0.0 ? " << endl;
  if(Q > 0.0 && q0 != q3) {
    energy = (Photon.dot(InParticle))/Q + Q*(InParticle.E() - InParticle.pz())/(q0-q3);
    px = qt*(InParticle.E() - InParticle.pz())/(q0-q3) -(q1*InParticle.px() + q2*InParticle.py())/qt;
    py = (q2*InParticle.px() - q1*InParticle.py())/qt;
    pz = (Photon.dot(InParticle))/Q;

    OutParticle.setE(energy);
    OutParticle.setPx(px);
    OutParticle.setPy(py);
    OutParticle.setPz(pz);

  }
  return OutParticle;
}



namespace Rivet {

  //store x, y, Q^2, tau as well as momenta of final state parameters in Breit and lab frame
  ofstream JetLabDataGroom("jetLabDataGroom.csv");
  ofstream JetBreitDataGroom("jetBreitDataGroom.csv");
  // store x, y, Q^2 and groomed tau and z_cut
  ofstream MetaDataGroom("MetaDataGroom.csv");
 

  /// @brief Add a short analysis description here
  class H1_GroomedES_Sherpa : public Analysis {
  public:
    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(H1_GroomedES_Sherpa);

    Histo2DPtr GMQ05,GMQ1,GMQ2,GMQ05norm,GMQ1norm,GMQ2norm,GrQTau05,GrQTau1,GrQTau2;
    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      declare(DISLepton(), "Lepton"); //scattered electron
      declare(DISKinematics(), "Kinematics"); //set of kinematic variables
      declare(FinalState(), "FS");//final state w/o scattered electron (?)
      vector<double> q2bins = {2.17600, 2.3000, 2.4500, 2.6500, 2.8500, 3.0500, 3.2500, 3.5500, 4.0000, 4.4437};
      vector<double> GIMbins = {-9,-5,-4,-3,-2,-1,0,1,2,3,4}; //binning for groomed invariant mass
      vector<double> GrTaubins = {0.0, 0.05, 0.10, 0.15, 0.22, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};//binning for groomed 1-jettiness
      // Book histograms
      // specify custom binning
      book(_h["x"], "x", 25,0,1);
      book(_h["q2"], "q2",25,0,10000 );
      book(_h["y"], "y",25,0,1 );
      book(_h["w2"], "w2", 25,0,10000);
      //book(_h["w3"], "w3", 25,0,10000);
      
      book(_h["GIM05"], "GIM05", GIMbins);
      book(_h["GIM1"], "GIM1", GIMbins);
      book(_h["GIM2"], "GIM2", GIMbins);
      book(_h["GIM05norm"], "GIM05norm", GIMbins);
      book(_h["GIM1norm"], "GIM1norm", GIMbins);
      book(_h["GIM2norm"], "GIM2norm", GIMbins);
      book(_h["GrTau05"], "GrTau05",GrTaubins);
      book(_h["GrTau1"], "GrTau1", GrTaubins);
      book(_h["GrTau2"], "GrTau2", GrTaubins);
      book(_h["Failed05"], "Failed05",10,-4,4);
      book(_h["Failed1"], "Failed1", 10,-4,4);
      book(_h["Failed2"], "Failed2", 10,-4,4);

      book(GMQ05, "GMQ05",q2bins, GIMbins);
      book(GMQ1, "GMQ1",q2bins, GIMbins);
      book(GMQ2, "GMQ2",q2bins, GIMbins);
      book(GMQ05norm, "GMQ05norm",q2bins, GIMbins);
      book(GMQ1norm, "GMQ1norm",q2bins, GIMbins);
      book(GMQ2norm, "GMQ2norm",q2bins, GIMbins);
      book(GrQTau05, "GrQTau05",q2bins, GrTaubins);
      book(GrQTau1, "GrQTau1",q2bins, GrTaubins);
      book(GrQTau2, "GrQTau2",q2bins, GrTaubins);
    
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) { //Mainly adopted from H1_2000_S4129130
      cout << "analysis beginning" << endl;
      const DISKinematics& dk = apply<DISKinematics>(event, "Kinematics");
      if ( dk.failed() ){
	     vetoEvent;
      }
      const int orientation = dk.orientation(); //should be +1 for HERA convention of pbeam = +z, here is -1 due to EPcollider.in convention (changing it causes errors w/ cross-section somehow)
      const DISLepton& dl = apply<DISLepton>(event,"Lepton");
      if ( dl.failed() ){
	      vetoEvent;
      }
      FourMomentum eout = dl.out().momentum()*GeV; //Weird unit mixing, dl.out gives units of GeV but dl.in() gives MeV??? todo
      FourMomentum ein = dl.in().momentum()*GeV;
      FourMomentum proton = dk.beamHadron().momentum()*GeV;
      if(orientation == -1)
	{
	  ein.setPz(-ein.pz());
	  eout.setPz(-eout.pz());
	  proton.setPz(-proton.pz());
	}

      FourMomentum photon = ein-eout;
      double q2 = -photon.mass2();
      double x     = q2 / (2.*proton*photon);
      double y     = (proton * photon) / (proton* ein);
      double w2 = q2*(1-x)/x;

      const LorentzTransform breitboost = dk.boostBreit();
      const double enel = eout.E();
      const double thel = 180 - eout.angle(dl.in().mom())/degree;
      
      const FinalState& fs = apply<FinalState>(event, "FS");

      ConstGenParticlePtr dislepGP = dl.out().genParticle(); 
      Particles particles; 
      particles.reserve(fs.size());
      vector<PseudoJet> breitparticles; 
      breitparticles.reserve(fs.size());
      
      for(const Particle& p: fs.particles()) { //loop to make sure DIS lepton is excluded from FS, included by default
        ConstGenParticlePtr loopGP = p.genParticle();
        if (loopGP == dislepGP) 
	  {
	    continue;
	  }
	FourMomentum pmom = p.momentum();
	if(orientation == -1)
	  {pmom.setPz(-pmom.pz());}
	const FourMomentum breitMom = Lab2Breit(photon,pmom);
	breitparticles.push_back(PseudoJet(breitMom.px(),breitMom.py(),breitMom.pz(),breitMom.E()));
      }
      
      //Set of physics cuts: 50<E-Pz<60 (not applied here, is QED ISR on?), E_e > 11GeV, E_theta < 2.7 radians, 0.2 < y < 0.7, 150 < Q2 < 20000
      bool evcut[4];                                                                                                                                                                                              // E_e cut                                                                                                                                               
      evcut[0] = enel/GeV > 11;
      // y cut                                                                                                                                                     
      evcut[1] = inRange(y,0.2,0.7);
      // Q2 cut                                                                                                                                                                
      evcut[2] = inRange(q2,150,20000.);
      // E_theta cut (154.7 deg = 2.7 rad)                                                                                                                                                             
      evcut[3] = inRange(thel,0,154.7);
      // Veto if fails any cuts                                                                                                                                                     
      if ( !(evcut[0] && evcut[1] && evcut[2] && evcut[3]) ) 
	{
  //        VetoFile << "I ground 3" << endl;
	  vetoEvent;  
	}
      //cout << "event passed cuts " << endl;
      //cout << std::setprecision(6) <<"        ein.pz() = " << ein.pz() << "eout.pz() is " << eout.pz() << " proton.pz is " << proton.pz() << " photon.E is " << photon.E() << endl;
      //cout << " q2 = " << q2 << " x = " << x << " y = " << y << " w2 = " << w2 << endl;
           
      FourMomentum qJ =ein-eout+x*proton;
      FourMomentum qB = x*proton;
      FourMomentum qJBreit = Lab2Breit(photon,qJ);
      FourMomentum qBBreit = Lab2Breit(photon,qB);
      
      double Q2min = 150;
  
      fastjet::contrib::CentauroPlugin centauro_plugin(1E30,0,+1.);
      fastjet::JetDefinition jet_def(&centauro_plugin);
      
      //clustering process
      ClusterSequence clust_seq_breit(breitparticles, jet_def);
      vector<PseudoJet> jets_breit = clust_seq_breit.inclusive_jets(0.0);
      vector<PseudoJet> sortedBreitJets = sorted_by_rapidity(jets_breit);
      
      PseudoJet j, j1, j2;
      double zcut_vec[3] = {0.05,0.1,0.2};   // {0.03, 0.2, 0.4};      // {0.05,0.1,0.2};
      double GIM_vec[3] = {-1,-1,-1}; //vec for storing GIM at zcut = 0.05, 0.1, and 0.2
      double failed_vec[3] = {-1,-1,-1}; //vec for storing if event failed grooming at zcut = 0.05, 0.1, and 0.2 
      double GrTau_vec[3] = {-1,-1,-1}; //vec for storing Groomed Tau at zcut = 0.05, 0.1, and 0.2
      double GrTau_vec2[3] = {-1,-1,-1};

      j=sortedBreitJets[0]; 
     
     //before the grooming: calculate tau using both eqs. 23 and 24
     double Tau0 = 0.0; //for eq. 23
     double Tau1 = 0.0; //for eq. 24
     for(std::vector<int>::size_type k=0; k<j.constituents().size(); k++  )
     {
          FourMomentum part;
          part.setPE(j.constituents()[k].px(),j.constituents()[k].py(),j.constituents()[k].pz(),j.constituents()[k].E());
          if(part.eta() > 0)
          {
              Tau0 += part.dot(qBBreit);
             // continue;
          }
          else
 	  {
              Tau0 += part.dot(qJBreit);
             // continue;
	  }
	  if(part.pz() < 0 ){
              Tau1 -= part.pz();
          }
      }
      Tau0 *= 2./q2;
      Tau1= 1. - 2./sqrt(q2)*Tau1;

      
      //write both vales for the ungroomed tau to file
      MetaDataGroom << Tau0 << ", " << Tau1 << ", " << x << ", " << y << ", " << q2 << ", 0, 0. " << endl;
      for(std::vector<int>::size_type k=0; k<j.constituents().size(); k++  ){
            JetBreitDataGroom << Tau0 << ", "  << j.constituents()[k].eta() << ", " << j.constituents()[k].phi() << ", " << j.constituents()[k].theta() << ", " << j.constituents()[k].pt() << ", " << x << ", " << y << ", " << q2 << ", 0" << endl;
      }


      bool failed = false;
      //apply declustering and grooming process
      for(long unsigned int i = 0;i<3;i++) //loop over zcut values in zcut_vec
	{

	  double zcut = zcut_vec[i];
	  
	  if(!jets_breit.empty() && !(sortedBreitJets.size() == 0))
	    {
	       j=sortedBreitJets[0];//da war es vorher
	     

	        
	      while (j.has_parents(j1,j2))
		{
		  double z1 = (j1.e()-j1.pz())/sqrt(q2);
		  double z2 = (j2.e()-j2.pz())/sqrt(q2);
		  if (min(z1,z2)/(z1+z2) > zcut ) //grooming passed! calculate event shape values
		    {
		      //	      cout << "Grooming passed" << endl;
		      double GrTau=0,GIM=0;
		      double GrTau1 = 0.;
		      vector<PseudoJet> constituents = j.constituents();
		      for(long unsigned int i = 0; i < constituents.size();i++)
			{
			  FourMomentum part;
			  part.setPE(constituents[i].px(),constituents[i].py(),constituents[i].pz(),constituents[i].E());
			  if(part.eta() > 0)
			    {
			      GrTau += part.dot(qBBreit);
			     // continue;
			    }
			  else
			    {
			      GrTau += part.dot(qJBreit);
			     // continue;
			    }
			  if(part.pz() < 0.){
                              GrTau1 -= part.pz();
		//	      VetoFile << part.pz() << endl;
		          }
			}
		      double GrTau2 = 1. - 2./sqrt(q2)*GrTau1;
		      GrTau = (2/q2)*GrTau;
		      //JetBreitDataGroom << GrTau << endl;
		      GrTau_vec[i] = GrTau;
		      GrTau_vec2[i] = GrTau2;
		      VetoFile << "GrTau0 " << GrTau << " GrTau1 " << GrTau2 << endl;
		      GIM_vec[i] = j.m2()/Q2min;
		      failed_vec[i] = 1;
		      break;
		    }
		  else
		    {
		      if(z1>z2)
			{j = j1;}
		      else
			{j = j2;}
		      if(j.constituents().size() == 0 || j.constituents().size() == 1)
			{
			  //cout << "Grooming failed " << endl;
			  //failed = true;
			  GIM_vec[i] = 1E-9;
			  break;
			}
		    }
		}
            }
	      //-----------------write z_cut and groomed values of tau to file 
	    MetaDataGroom << GrTau_vec[i] << ", " << GrTau_vec2[i] << ", " << x << ", " << y << ", " << q2 << ", " << i+1 << ", " << zcut << endl;
            for(std::vector<int>::size_type k=0; k<j.constituents().size(); k++  ){

                              JetBreitDataGroom << GrTau_vec[i] << ", "  << j.constituents()[k].eta() << ", " << j.constituents()[k].phi() << ", " << j.constituents()[k].theta() << ", " << j.constituents()[k].pt() << ", " << x << ", " << y << ", " << q2 << ", " << i+1 << endl;
             }
//--------------------------
	   // }
	}
      if(!failed)  
	{
	  
          
	  _h["q2"]->fill(q2);
	  _h["y"]->fill(y);
	  _h["x"]->fill(x);
	  _h["w2"]->fill(w2);

	  //cout << "after 1st histos 1D " << endl;
	  //cout << " obs:  GIM "<< GIM_vec[0]<<" "<<GIM_vec[1]<<" "<<GIM_vec[2]<<endl;
	  //cout << " obs:  GrTau "<<GrTau_vec[0]<<" "<<GrTau_vec[1]<<" "<<GrTau_vec[2]<<endl;
	  
	  for (size_t k=0;k<3;k++) {
	    if (GIM_vec[k]==-1) GIM_vec[k] = 1E-9; 
	  }

	  //cout << " obs:  GIM "<< GIM_vec[0]<<" "<<GIM_vec[1]<<" "<<GIM_vec[2]<<endl;
	  
	  _h["GIM05"]->fill(log(GIM_vec[0]));
	  _h["GIM1"]->fill(log(GIM_vec[1]));
	  _h["GIM2"]->fill(log(GIM_vec[2]));
	  _h["GIM05norm"]->fill(log(GIM_vec[0]*Q2min/q2));
	  _h["GIM1norm"]->fill(log(GIM_vec[1]*Q2min/q2));
	  _h["GIM2norm"]->fill(log(GIM_vec[2]*Q2min/q2));
	  _h["GrTau05"]->fill(GrTau_vec[0]);
	  _h["GrTau1"]->fill(GrTau_vec[1]);
	  _h["GrTau2"]->fill(GrTau_vec[2]);

	  //cout << "pre fail histos 1D " << endl;
	  
	  _h["Failed05"]->fill(failed_vec[0]);
	  _h["Failed1"]->fill(failed_vec[1]);
	  _h["Failed2"]->fill(failed_vec[2]);

	  //cout << "after histos 1D " << endl;
	  double binmin = 0;
	  vector<double> q2bins = {2.17600, 2.3000, 2.4500, 2.6500, 2.8500, 3.0500, 3.2500, 3.5500, 4.0000, 4.4437};
	  for(long unsigned int i = 0; i < q2bins.size()-1; i++)
	    {
	      if(log10(q2) > q2bins[i] && log10(q2) < q2bins[i+1])
		{
		  binmin = pow(10,q2bins[i]);
		}
	    }
	  GMQ05->fill(log10(q2),log(GIM_vec[0]*Q2min/binmin));
	  GMQ1->fill(log10(q2),log(GIM_vec[1]*Q2min/binmin));
	  GMQ2->fill(log10(q2),log(GIM_vec[2]*Q2min/binmin));
	  GMQ05norm->fill(log10(q2),log(GIM_vec[0]*Q2min/q2));
	  GMQ1norm->fill(log10(q2),log(GIM_vec[1]*Q2min/q2));
	  GMQ2norm->fill(log10(q2),log(GIM_vec[2]*Q2min/q2));
	  GrQTau05->fill(log10(q2),GrTau_vec[0]);
	  GrQTau1->fill(log10(q2),GrTau_vec[1]);
	  GrQTau2->fill(log10(q2),GrTau_vec[2]);
	  //cout << "after histos 2D " << endl;
	}
    }

    /// Normalise histograms etc., after the run
    void finalize() {
      cout << "Cross section is " << crossSection() << endl;
     
      scale(_h["GIM05"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(_h["GIM1"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(_h["GIM2"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(_h["GIM05norm"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(_h["GIM1norm"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(_h["GIM2norm"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(_h["GrTau05"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(_h["GrTau1"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(_h["GrTau2"], crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GMQ05, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GMQ1, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GMQ2, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GMQ05norm, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GMQ1norm, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GMQ2norm, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GrQTau05, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GrQTau1, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
      scale(GrQTau2, crossSection()/nanobarn/sumW()); // norm to generated cross-section in nb (after cuts)
    }

    //@}


    /// @name Histograms
    //@{
    map<string, Histo1DPtr> _h;
    map<string, Profile1DPtr> _p;
    map<string, CounterPtr> _c;
    //@}


  };


  DECLARE_RIVET_PLUGIN(H1_GroomedES_Sherpa);

}
